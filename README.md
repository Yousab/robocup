# RoboCup


## Prerequisites
- PHP v.7.2
- composer installed
- MySQL/SQLITE
- Apache/IIS supported
- Postman (to check REST APIs)



## How to install
1. Download any of the two zip files.
2. Unextract the zip file and place it in your local webserver document root (for example for Apache webserver: /var/www/html/[robocup], and you can access it through http://localhost/[robocup]) or place it anywhere and create a virtual host as following (Apache webserver supported, custom configuration dependency):
	- create a new file in /etc/apache2/sites-available/[conf_name].conf
	- Write down your virtualhost apache configuration as in `virtualhost.conf` file attached here.
	- Add your new server name in /etc/hosts file, then enable the site `sudo a2ensite [conf_name].conf`, and restart the web server `sudo service apache2 restart`
	- ### Small note here that either of the projects are supported to run over IIS server, there is `web.config` file in the public directory to support the IIS web server.
3. Run composer install on the repo root directory to install project dependencies(`npm install`).
4. Browse to `http://[server_name as configured]` to make sure everything is running as expected.



## Importing the DB

### For the Laravel Project
1. You can import "robocupdb.sql" file which located in the project root directory to your MySQL DB through one of the following ways (the file containes the DB schema creation):
	- Through CLI by running this command on the project root directory path: `mysql -u [username] -p < robocupdb.sql`
	- Through PHPMyAdmin, select the "Import" tab and upload the sql file and click Import.
	- Through MySQL Workbench, after connecting to the server, from the Server tab on top menu, Choose "Data Import" and import the desired file.

### For the Laravel/Lumen Project
1. This db is up and running if you use the same .env.robocup.example with predefined configurations. It depends on `robocupdb.sqlite` which located under the database directory.
	
	(Note)	You can run db migrations as well in this project to MySQL, but we have to update the .env db connection as per that.
	
	
	
## For the .env file
There is a file named `.env.robocup.example` in each of the projects,  So we can copy it to `.env` file or udpate any config values if needed.

## Import Postman Collection
To check REST APIs for either of the projects, You can check the endpoints easily by importing the collection json file which tagged as postman collection to the Postman app. For example in the "robocup_lumen" prject, you can find `Robocup - Lumen.postman_collection.json` file in the root directory.

Or use the following links to import immediately to your postman app:

	- Laravel Robocup collection: https://www.getpostman.com/collections/5fda3b73c72d9bd09053
	- Laravel/Lumen Robocup collection: https://www.getpostman.com/collections/f109f7b911db1269eca7
	
	

## Issues while installing
1. Sometimes there is a permission issue on project folders especially (storage/framework and bootstrap directories), So make sure to give them the correct file and folder permissions. I was giving the apache user permission to write by running the following commands:
	- `sudo chown -R :www-data storage/framework bootstrap/`
	- `sudo chmod -R g+w storage/frameowrk/ bootstrap/`
	
 	OR on windows, we have to give the IIS user the same permissions as mentioned above, So you can accmoplish that by the following points:
	
	- Right click on (storage or bootstrap) folder, Click Properties
	- Go to the security tab, then Press the edit button
	- Add IUSR and give it the Full Control, then apply and save